import React, { useState, useEffect } from 'react';
import PokemonList from './PokemonList';
import axios from 'axios';
import Pagination from './Pagination';

import { Layout } from 'antd';

export default function Content() {
    const [pokemon, setPokemon] = useState([])
    const [data, setData] = useState([])
    const [currentPageUrl, setCurrentPageUrl] = useState("https://pokeapi.co/api/v2/pokemon")
    const [nextPageUrl, setNextPageUrl] = useState()
    const [prevPageUrl, setPrevPageUrl] = useState()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setLoading(true)
        let cancel
        axios.get(currentPageUrl, {
            cancelToken: new axios.CancelToken(c => cancel = c)
        }).then(res => {
            setLoading(false)
            setNextPageUrl(res.data.next)
            setPrevPageUrl(res.data.previous)
            setPokemon(res.data.results.map(p => p.name))
            setData(res.data.results.map(p => paramProps(p.url)))
        })

        return () => cancel()
    }, [currentPageUrl])

    function paramProps(link)
	{
        let param = link.replace(/\D/g, "");
        let params = param.slice(1);
		return params;
	}

    function gotoNextPage() {
        setCurrentPageUrl(nextPageUrl)
    }

    function gotoPrevPage() {
        setCurrentPageUrl(prevPageUrl)
    }

    if (loading) return "Loading..."

    return (
        <Layout style={{ padding: '0 50px', marginTop: 70 }}>
            <PokemonList pokemon={pokemon} url={data}/>
            <Pagination
                gotoNextPage={nextPageUrl ? gotoNextPage : null}
                gotoPrevPage={prevPageUrl ? gotoPrevPage : null}
            />
        </Layout>
    );
}
