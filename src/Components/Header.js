import React from 'react';
import '../App.css';
import logo from '../logo.svg';
import { Layout } from 'antd';

const { Header } = Layout;

export default function Top() {
  return (
    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
        <img src={logo} className="App-logo" alt="logo" /> Simple Pokedex
    </Header>
  );
}
